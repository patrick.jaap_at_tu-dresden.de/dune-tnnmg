#ifndef SCALAR_TNNMG_HH
#define SCALAR_TNNMG_HH

#include <vector>
#include <string>

#define MMG_ENABLE_EXACT_OBSTACLES

#ifdef MMG_ENABLE_EXACT_OBSTACLES
#include <map>
#endif


#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bcrsmatrix.hh>


#include <dune/solvers/transferoperators/truncatedcompressedmgtransfer.hh>
#include <dune/solvers/norms/norm.hh>

#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/problem-classes/onedconvexfunction.hh>
#include <dune/tnnmg/problem-classes/convexproblem.hh>

class ScalarTNNMG
{
	public:
		
		typedef Dune::BCRSMatrix< Dune::FieldMatrix<double,1,1> > Matrix;
		typedef Dune::BlockVector< Dune::FieldVector<double,1> > Vector;
		typedef TruncatedCompressedMGTransfer< Vector > Transfer;
		typedef Nonlinearity< Dune::FieldVector<double,1>, Dune::FieldMatrix<double,1,1> > ScalarNonlinearity;
		typedef OneDConvexFunction<ScalarNonlinearity> ConvexFunction;
		
		ScalarTNNMG(std::vector<Transfer*>& _transfer, const Dune::ParameterTree& _config);
		
		void solve(double a1, const Matrix& _mat, double a2, const Vector& _mat_vec, Vector& _u, const Vector& _rhs,
				ScalarNonlinearity& phi, const Norm<Vector> &norm,
				int _fine_level, int _coarse_level, const Dune::BitSetVector<1>& isHanging, const Dune::BitSetVector<1>& _isDirichlet, Vector* u_exact = 0, std::vector<Transfer*>* transfer2 = 0);
		
		void solve(ConvexProblem<>& problem, const Norm<Vector> &norm,
				int _fine_level, int _coarse_level, const Dune::BitSetVector<1>& isHanging, const Dune::BitSetVector<1>& _isDirichlet, Vector* u_exact = 0, std::vector<Transfer*>* transfer2 = 0);
		
		
		void nonlinearGaussSeidel(ScalarNonlinearity& phi, const Dune::BitSetVector<1>& isHanging);
		void nonlinearJacobi(ScalarNonlinearity& phi, const Dune::BitSetVector<1>& isHanging);

		void assemble(const ScalarNonlinearity& phi, int& truncated, const Dune::BitSetVector<1>& isHanging);
		
		void v_cycle(bool modifytruncation);
		void smooth();
		void smoothConst();
		
		
		void restrictObstacles(const Vector& upperFine, Vector& upperCoarse, const Vector& lowerFine, Vector& lowerCoarse, const Dune::BitSetVector<1> &contact, const Matrix& op) const;
		
		void findContactSet(const Vector& u_new, const Vector& u_old, const Vector& upper, const Vector& lower,
							Dune::BitSetVector<1>& contact, int& contact_count, int& contact_diff);
		
		
		double linesearch(const Vector& u, Vector& v, ScalarNonlinearity& phi);
		
		/** \brief minimize convex functional
		* 
		* Computes an approximate minimizer of the convex functional
		* @f[ f(x) = \frac 1 2 a^2x - rx + \phi(x) @f]
		* using bisection. If \f$ x^* \f$ is the exact minimizer
		* and \f$ x^{old} \f$ is the old value the returned
		* approximation @f$ x @f$ satisfies
		* \f[
		* (x - x^{old}) \in [ factor , 1] (x^*- x^{old}).
		* \f]
		* This guarantees enough descent for every fixed \f$ factor \f$ and hence convergence of the Gauss-Seidel method.
		*
		* \param A quadratic part
		* \param r linear part
		* \param i component
		* \param phi non-smooth part
		* \param x initial value
		* \param old old value, needed for inexact minimization
		* \param factor acceptance factor for inexact minimization
		* \return approximate minimizer
		*/
		double minimize(double A, double r, int i, const ScalarNonlinearity& phi, double x, double old, double factor, double tol, double safety);
		
		int iter;
		double err;
		
		int maxIter;
		double tol;
		
		double obst_tol;
		bool contactOnly;
		
		std::string indent;
	
	private:

		struct CycleType
		{
			int finepre;
			int finepost;
			bool linearfinesmoothing_flag;
			int pre;
			int post;
			int coarse;
		};

        struct SmootherType
        {
            bool jacobi_flag;
            bool sgs_flag;
            double gs_acceptance;
            double gs_tol;
            double gs_safety;
            bool nonlinear_jacobi_flag;
            bool damp_fine_smoother_flag;
        };

		struct LineSearchType
		{
			bool enable;
			double acceptance;
			double tol;
			double safety;
		};

        struct ExperimentalFlags
        {
            bool linesearch_damponly;
            bool linesearch_normed;
            bool linesearch_startbyone;
            bool linesearch_descentonly;
        };


		const Dune::ParameterTree& config;
		
		std::vector<Transfer*>& transfer;
		std::vector<Transfer*>* transfer2;
		
		std::vector< Matrix > A;
		std::vector< Vector > m;
		std::vector< Vector > v;
		std::vector< Vector > v_sum;
		std::vector< Vector > res;
		std::vector< Vector > upper;
		std::vector< Vector > lower;
		std::vector< Dune::BitSetVector<1> > truncationlist;
		std::vector< Dune::BitSetVector<1> > modificationlist;
		std::vector< Dune::BitSetVector<1> > isDirichlet;
		
		Dune::BitSetVector<1> truncation;
		
		Vector u;
		
		Matrix mat;
		Vector mat_vec;
		Vector rhs;
		
		int fine_level;
		int coarse_level;
		int level;
		
		CycleType cycleType;
		LineSearchType linesearchType;
        SmootherType smootherType;
        ExperimentalFlags experimentalFlags;

		
		
		bool truncate_flag;
		bool allowcoarseviolation_flag;
		bool projectcorrection_flag;
		int gsprojectsteps;
		bool truncatefineonly_flag;
		bool convexcombination_flag;
		bool activesetvcycle_flag;
		bool adaptiveactivesetvcycle_flag;
		
		enum {monotone, none, exact, apriori} coarseobstacles;

		double regularity_tol;
		
		bool cg_flag;
		bool additiv_flag;
		
		
		bool fast_quadratic_minimize_flag;
		bool simplegs;
		bool bisection_flag;
		
		int bisectionsteps;
		
		
		int verbosity;
		bool print_smoothing_time_flag;
		
#ifdef MMG_ENABLE_EXACT_OBSTACLES
		std::vector< std::vector< std::map<int, double> > > fineLevelInterpolation;
		Vector actual_upper;
		Vector actual_lower;
#endif

};

//#include "scalartnnmg.cc"
#endif
