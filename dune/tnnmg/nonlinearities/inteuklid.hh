#ifndef INTEUKLID_HH
#define INTEUKLID_HH

/** \file 
 * \brief Implements 0.5 times the Euclidean norm squared times
 */

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include "smallfunctional.hh"

/** \brief Implements 0.5 times the Euclidean norm squared
 * \tparam dimension The vector space dimension
 */
template<int dimension>
class Euklid : public SmallFunctional< dimension >
{
    public:
        
        /** \brief The type used for vector blocks */
        typedef typename Euklid< dimension >::SmallVector SmallVector;
        
        /** \brief The type used for matrix blocks */
        typedef typename Euklid< dimension >::SmallMatrix SmallMatrix;

        /** \brief Return \f$ \frac{1}{2} \Vert v \Vert_2^2 \f$ */
        double operator()(const SmallVector v) const
        {
            return 0.5 * v.two_norm2();
        }

        /** \brief The gradient of 0.5 times the Euclidean norm squared, which is just the argument v itself */
        SmallVector d(const SmallVector v) const
        {
            return v;
        }

        /** \brief The Hessian of 0.5 times the norm squared, which is the identity */
        SmallMatrix d2(const SmallVector v) const
        {
            SmallMatrix H(0.0);
            for (int row=0; row < SmallMatrix::rows; ++row)
                H[row][row] = 1.0;
            return H;
        }

};

#endif
