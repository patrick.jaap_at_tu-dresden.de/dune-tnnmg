#ifndef ZERO_NONLINEARITY_HH
#define ZERO_NONLINEARITY_HH

/** \file
 * \brief A dummy nonlinearity class representing the zero function
 */

#include <dune/solvers/common/interval.hh>
#include "dune/tnnmg/problem-classes/pointwisenonlinearity.hh"

/** \brief A nonlinearity representing the zero function
 */
template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
class ZeroNonlinearity: public PointwiseNonlinearity<LocalVectorType, LocalMatrixType>
{
    public:
        typedef Nonlinearity<LocalVectorType, LocalMatrixType> NonlinearityType;

        using NonlinearityType::block_size;

        typedef typename NonlinearityType::VectorType VectorType;
        typedef typename NonlinearityType::MatrixType MatrixType;
        typedef typename NonlinearityType::IndexSet IndexSet;


        ZeroNonlinearity()
        {}

        /** \brief Returns zero */
        double operator()(const VectorType& v) const
        {
            return 0.0;
        }

        void addGradient(const VectorType& v, VectorType& gradient) const {}
        void addHessian(const VectorType& v, MatrixType& hessian) const {}
        void addHessianIndices(IndexSet& indices) const {}

        /** \brief Returns the interval \f$ [0,0]\f$ */
        void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
        {
            D[0] = 0.0;
            D[1] = 0.0;
        }

        /** \brief Returns 0 */
        double regularity(int i, double x, int j) const
        {
            return 0.0;
        }

        void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
        {
            dom[0] = -std::numeric_limits<double>::max();
            dom[1] = std::numeric_limits<double>::max();
            return;
        }
};

#endif

