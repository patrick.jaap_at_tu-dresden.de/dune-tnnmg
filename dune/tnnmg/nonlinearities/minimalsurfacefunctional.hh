#ifndef __MINIMALSURFACEFUNCTIONAL__HH__
#define __MINIMALSURFACEFUNCTIONAL__HH__

#include <cmath>
#include "smallfunctional.hh"

//! The map \f$ x \mapsto (|x|^2 + 1)^{1/2}\f$
template<int dimension>
class MinimalSurfaceFunctional : public SmallFunctional< dimension >
{
	public:
		typedef typename SmallFunctional< dimension >::SmallVector SmallVector;
		typedef typename SmallFunctional< dimension >::SmallMatrix SmallMatrix;

    /** \brief Evaluate value of the functional */
		double operator()(const SmallVector v) const
		{
			return std::sqrt(1.0+v.two_norm2());
		}

    /** \brief Evaluate gradient of the functional */
		SmallVector d(const SmallVector v) const
		{
			double value = (*this)(v);
			
			SmallVector D = v;
			D /= value;
			return D;
		}

    /** \brief Evaluate Hessian of the functional */
		SmallMatrix d2(const SmallVector v) const
		{
			double value = (*this)(v);
			double value2 = value * value;
	
			SmallMatrix H;
			for (int i=0; i < SmallVector::dimension; ++i)
			{
				for (int j=0; j < SmallVector::dimension; ++j)
				{
					if (i==j)
						H[i][j] = (1.0 - v[i]*v[j]/value2)/value;
					else
						H[i][j] = (- v[i]*v[j]/value2)/value;
				}
			}
			return H;
		}
};

#endif

