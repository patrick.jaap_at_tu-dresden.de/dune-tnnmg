add_subdirectory(test)

install(FILES
    inteuklid.hh
    localnonlinearity.hh
    minimalsurfacefunctional.hh
    obstpotential.hh
    pnorm.hh
    shiftednonlinearity.hh
    smallfunctional.hh
    smoothl1.hh
    sumnonlinearity.hh
    zerononlinearity.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/tnnmg/nonlinearities)
