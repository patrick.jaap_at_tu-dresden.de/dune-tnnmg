#ifndef LOCALNONLINEARITY_HH
#define LOCALNONLINEARITY_HH

#include <dune/common/fmatrix.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include "dune/tnnmg/problem-classes/nonlinearity.hh"
#include "smallfunctional.hh"

template <int dimension>
class LocalNonlinearity: public Nonlinearity< Dune::FieldVector<double,1>, Dune::FieldMatrix<double,1,1> >
{
    public:
        typedef Dune::BlockVector< Dune::FieldVector<double,1> > Vector;
        typedef Dune::BCRSMatrix< Dune::FieldMatrix<double,1,1> > Matrix;
        typedef Dune::BCRSMatrix< Dune::FieldMatrix< double, dimension, 1 > > GradientMatrix;
        typedef Dune::BCRSMatrix< Dune::FieldMatrix< double, 1, dimension > > TransposedGradientMatrix;
        typedef Dune::BlockVector< Dune::FieldVector< double, dimension > > GradientVector;

        LocalNonlinearity(const GradientMatrix& _B, const TransposedGradientMatrix& _Bt, const Vector& _weights, const SmallFunctional<dimension>& _gamma, double _TOL = 1e-15) :
            B(_B),
            Bt(_Bt),
            weights(_weights),
            gamma(_gamma),
            TOL(_TOL)
        {}


        double operator()(const Vector& v) const
        {
            double r = 0.0;
            GradientVector dv(B.N());
            B.mv(v,dv);
            for(int row=0; row<dv.size(); ++row)
            {
                r += gamma(dv[row]) * weights[row];
            }
            return r;
        }

        void addGradient(const Vector& v, Vector& gradient) const
        {
            Dune::FieldVector<double, dimension> gammaBuj;
            Dune::FieldVector<double, dimension> Buj;

            typename GradientMatrix::row_type::ConstIterator it;
            typename GradientMatrix::row_type::ConstIterator end;

            double r;
            for (int row=0; row<weights.size(); ++row)
            {
                it  = B[row].begin();
                end = B[row].end();
                Buj = 0.0;
                for (; it!=end; ++it)
                    (*it).umv(v[it.index()], Buj);

                gammaBuj = gamma.d(Buj);
                gammaBuj *= weights[row];

                it = B[row].begin();
                for (; it!=end; ++it)
                {
                    r = 0.0;
                    for (int i=0; i < dimension; ++i)
                        r += (*it)[i][0] * gammaBuj[i];

                    gradient[it.index()] += r;
                }
            }
        }

        void addHessian(const Vector& v, Matrix& hessian) const
        {
            Dune::FieldMatrix< double, dimension, dimension > gammaBvj;
            Dune::FieldVector<double, dimension> Bvj;

            typename GradientMatrix::row_type::ConstIterator i;
            typename GradientMatrix::row_type::ConstIterator j;
            typename GradientMatrix::row_type::ConstIterator end;

            double r;
            for(int row=0; row<weights.size(); ++row)
            {
                i  = B[row].begin();
                end = B[row].end();
                Bvj = 0.0;
                for (; i!=end; ++i)
                    (*i).umv(v[i.index()], Bvj);

                gammaBvj = gamma.d2(Bvj);
                gammaBvj *= weights[row];

                i  = B[row].begin();
                for(; i!=end; ++i)
                {
                    j  = B[row].begin();
                    for(; j!=end; ++j)
                    {
                        // hessian[i.index()][j.index()] += (*i)' * gammaBvj * (*j);

                        r = 0.0;
                        for(int k=0; k<dimension; ++k)
                            for(int l=0; l<dimension; ++l)
                                r += gammaBvj[k][l] * (*j)[l][0] * (*i)[k][0];

                        hessian[i.index()][j.index()][0][0] += r;
                    }
                }
            }
        }

        void addHessianIndices(IndexSet& indices) const
        {
            typename GradientMatrix::row_type::ConstIterator i;
            typename GradientMatrix::row_type::ConstIterator j;
            typename GradientMatrix::row_type::ConstIterator end;

            for(int row=0; row<weights.size(); ++row)
            {
                i  = B[row].begin();
                end = B[row].end();
                for(; i!=end; ++i)
                {
                    j  = B[row].begin();
                    for(; j!=end; ++j)
                        indices.add(i.index(), j.index());
                }
            }
        }

        void directionalSubDiff(const Vector& u, const Vector& v, Dune::Solvers::Interval<double>& D)
        {
            Dune::FieldVector<double, dimension> gammaBuj;
            Dune::FieldVector<double, dimension> Buj;

            typename GradientMatrix::row_type::ConstIterator it;
            typename GradientMatrix::row_type::ConstIterator end;

            double r;
            double d = 0.0;
            for (int row=0; row<weights.size(); ++row)
            {
                it  = B[row].begin();
                end = B[row].end();
                Buj = 0.0;
                for (; it!=end; ++it)
                    (*it).umv(u[it.index()], Buj);

                gammaBuj = gamma.d(Buj);
                gammaBuj *= weights[row];

                it = B[row].begin();
                for (; it!=end; ++it)
                {
                    r = 0.0;
                    for (int i=0; i < dimension; ++i)
                        r += (*it)[i][0] * gammaBuj[i];

                    d += r * v[it.index()];
                }
            }
            D[0] = d;
            D[1] = d;
        }

        void setVector(const Vector& v)
        {
            Bu.resize(B.N());
            u = v;
            Bu = 0.0;
            B.umv(u, Bu);
        }

        void updateEntry(int col, double u_new, int j)
        {
            typename TransposedGradientMatrix::row_type::ConstIterator colIt;
            typename TransposedGradientMatrix::row_type::ConstIterator colEnd;
            int row;
            colIt  = Bt[col].begin();
            colEnd = Bt[col].end();
            for(; colIt!=colEnd; ++colIt)
            {
                row = colIt.index();

                for (int i=0; i < dimension; ++i)
                    Bu[row][i] += (*colIt)[0][i] * (u_new - u[col]);
            }
            u[col] = u_new;
        }

        void subDiff(int col, double x, Dune::Solvers::Interval<double>& D, int j) const
        {
            Dune::FieldVector<double, dimension> gammaBuj;
            Dune::FieldVector<double, dimension> Buj;

            typename TransposedGradientMatrix::row_type::ConstIterator colIt;
            typename TransposedGradientMatrix::row_type::ConstIterator colEnd;

            int row, i;
            double r = 0.0;
            double u_diff = x - u[col];

            colIt  = Bt[col].begin();
            colEnd = Bt[col].end();
            for(; colIt!=colEnd; ++colIt)
            {
                row = colIt.index();

                Buj = Bu[row];
                for (i=0; i < dimension; ++i)
                    Buj[i] += u_diff * (*colIt)[0][i];

                gammaBuj = gamma.d(Buj);
                gammaBuj *= weights[row];

                for (i=0; i < dimension; ++i)
                    r += (*colIt)[0][i] * gammaBuj[i];
            }
            D[0] = r;
            D[1] = r;
        }

        double regularity(int i, double x, int j) const
        {
            return 0.0; 
        }

        void domain(int i, Dune::Solvers::Interval<double>& D, int j) const
        {
            D[0] = -std::numeric_limits<double>::max();
            D[1] = std::numeric_limits<double>::max();
        }

    private:
        GradientVector Bu;
        const GradientMatrix& B;
        const TransposedGradientMatrix& Bt;
        const Vector& weights;
        const SmallFunctional<dimension>& gamma;
        const double TOL;

//      Vector* u;
        Vector u;
};


#endif
