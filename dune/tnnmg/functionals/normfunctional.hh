#ifndef DUNE_TNNMG_FUNCTIONALS_NORMFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_NORMFUNCTIONAL_HH

#include <dune/common/bitsetvector.hh>
#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/functionals/nonsmoothconvexfunctional.hh>

namespace Dune {

  namespace TNNMG {

    /** \brief The weighted sum of the Euclidean norm of the vector blocks
     *
     * This Functional class implements the functional
     * \f[
     *    j(v) = \sum_{i=1}^n a_i |v_i|,
     * \f]
     *where \f$ v \f$ is a vector in \f$ (\mathbb{R}^N)^n \f$, and \f$ a \in \mathbb{R}^n \f$
     * is a vector of coefficients.
     *
     * \tparam blockSize The size \f$ N \f$ of the vector blocks.
     */
    template <int blockSize>
    class NormFunctional
    : public Dune::TNNMG::NonsmoothConvexFunctional<FieldVector<double,blockSize>,
                                                    FieldMatrix<double,blockSize,blockSize> >
    {
    public:
      typedef Dune::FieldVector<double,blockSize>      LocalVectorType;
      typedef Dune::FieldMatrix<double,blockSize,blockSize>  LocalMatrixType;

      typedef Nonlinearity<LocalVectorType, LocalMatrixType> NonlinearityType;

      typedef typename NonlinearityType::VectorType VectorType;
      typedef typename NonlinearityType::MatrixType MatrixType;
      typedef typename NonlinearityType::IndexSet IndexSet;


      /** \brief Constructor from a set of coefficients
       *
       * \param coefficients The coefficient vector \f$ a \f$
       * \param tolerance The nonsmoothness a 0 of the norm function is implemented to have the radius 'tolerance'
       */
      NormFunctional(const std::vector<double> coefficients,
                     double tolerance = 1e-15)
      : coefficients_(coefficients),
      tolerance_(tolerance)
      {}

      /** \brief Evaluate the functional at a given vector v
       */
      double operator()(const VectorType& v) const
      {
        double result = 0.0;

        for(size_t row=0; row<v.size(); ++row)
          result += coefficients_[row] * v[row].two_norm();

        return result;
      }

      /** \brief Add the gradient of the norm functional to a given vector
       *
       * Note: the gradient of \f$ |v| \f$ is \f$ v/|v| \f$.
       */
      void addGradient(const VectorType& v, VectorType& gradient) const
      {
        for(size_t row=0; row<v.size(); ++row)
        {
          double norm = v[row].two_norm();

          // Do nothing if we are at the nondifferentiable point
          if (norm < tolerance_)
            continue;

          for (int j=0; j<blockSize; j++)
            gradient[row][j] += coefficients_[row] * v[row][j] / norm;
        }
      }

      /** \brief Add the Hessian of the functional to a given matrix
       *
       * Note: Entry \f$ i,j \f$ of the Hessian of \f$ |v| \f$  is \f[- \frac{v_i v_j}{|v|^3} + \frac{\delta_{ij}}{|v|} \f]
       */
      void addHessian(const VectorType& v, MatrixType& hessian) const
      {
        for(size_t row=0; row<v.size(); ++row)
        {
          double norm = v[row].two_norm();

          // Do nothing if we are at the nondifferentiable point
          if (norm < tolerance_)
            continue;

          for (int i=0; i<blockSize; i++)
            for (int j=0; j<blockSize; j++)
              hessian[row][row][i][j] += coefficients_[row] * (- v[row][i] * v[row][j] / (norm*norm*norm) + (i==j) / norm);
        }
      }

      void addHessianIndices(IndexSet& indices) const {}

      /** \brief Compute the subdifferential of the nonlinearity restricted to the line \f$ u + t v \f$.
       *
       * \param[out] subdifferential the resulting subdifferential
       * \param u base point
       * \param v direction
       */
      virtual void directionalSubDiff(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& subdifferential) const
      {
        subdifferential[0] = 0.0;
        subdifferential[1] = 0.0;

        for(size_t row=0; row<u.size(); ++row)
        {
          double norm = u[row].two_norm();
          if (norm >= tolerance_){
            subdifferential[0] += coefficients_[row] * (u[row]*v[row]) / norm;
            subdifferential[1] += coefficients_[row] * (u[row]*v[row]) / norm;
          } else {
            subdifferential[0] -= coefficients_[row] * v[row].two_norm();
            subdifferential[1] += coefficients_[row] * v[row].two_norm();
          }
        }
      }

      /** \brief Compute the subdifferential of the nonlinearity restricted to the
       * line u_pos' +t e_(i,j) at t=0.
       *
       * Here e_(i,j) is the (i,j)-th Euclidean unit vector,
       * and u_pos' is the internal position vector u_pos with the (i,j)-the entry replaced by x.
       * If the nonlinearity decouples in the Euclidean directions this is simply the (i,j)-th
       * component of the subdifferential.
       *
       * \param i global index
       * \param j local index
       * \param x value of the (i,j)-th entry of position to evaluate the nonlinearity at
       * \param[out] D the subdifferential
       */
      virtual void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
      {
        D[0] = 0.0;
        D[1] = 0.0;

        // Compute current tangential displacement
        LocalVectorType displacement = u_[i];
        displacement[j] = x;
        double norm = displacement.two_norm();
        if (norm < tolerance_)
        {
          D[0] = - coefficients_[i];
          D[1] =   coefficients_[i];
        } else {
          D[0] = D[1] = coefficients_[i] * displacement[j] / norm;
        }
      }

      /** \brief Return the regularity of the nonlinearity restricted to the
       * line u_pos' +t e_(i,j) at t=0.
       *
       * Here e_(i,j) is the (i,j)-th Euclidean unit vector,
       * and u_pos' is the internal position vector u_pos with the (i,j)-the entry replaced by x.
       * Usually this will be the third derivative or a local Lipschitz constant of the second
       * derivative. Note that if the subdifferential is set-valued at this position, this
       * value will normally be infinity.
       *
       * \param i global index
       * \param j local index
       * \param x value of the (i,j)-th entry of position to evaluate the nonlinearity at
       * \returns a value measuring the regularity
       */
      virtual double regularity(int i, double x, int j) const
      {
        // Compute current tangential displacement
        LocalVectorType displacement = u_[i];
        displacement[j] = x;

        return (displacement.two_norm() < tolerance_)
        ? std::numeric_limits<double>::max()
        : 0;
      }

      /** \brief Domain of definition of the functional in one coordinate direction
       *
       * The norm functional is the entire space
       */
      void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
      {
        // Our domain is the entire space
        dom[0] = -std::numeric_limits<double>::max();
        dom[1] =  std::numeric_limits<double>::max();
      }

      /** \brief Set the internal position vector u_pos to v.
       */
      virtual void setVector(const VectorType& v)
      {
        u_ = v;
      }

      /** \brief Update the (i,j)-th entry of the internal position vector u_pos to x.
       *
       *
       * \param i global index
       * \param j local index
       * \param x new value of the entry (u_pos)_(i,j)
       */
      virtual void updateEntry(int i, double x, int j)
      {
        u_[i][j] = x;
      }


    private:

      /** \brief For each block, the norm is weighted with the corresponding coefficient from this array */
      const std::vector<double> coefficients_;

      /** \brief We consider the norm function to be nondifferentiable a circle of this radius around 0 */
      const double tolerance_;

      /** \brief The current state */
      VectorType u_;

    };

  }

}
#endif

