// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <iostream>

#include <dune/tnnmg/functionals/normfunctional.hh>
#include <dune/tnnmg/functionals/test/functionaltest.hh>

using namespace Dune;

int main(int argc, char** argv)
{
  // Create a norm functional on (R^3)^2 for testing
  std::vector<double> coefficients = {2.0, 3.0};
  typedef TNNMG::NormFunctional<3> Functional;
  Functional functional(coefficients);

  // A set of local test points, i.e., values for a single vector block
  std::vector<Functional::LocalVectorType> localTestPoints = {{0,0,0},
                                                              {1,0,0},
                                                              {0,-2,0},
                                                              {3.14,3.14,-4}};

  // Create real test points (i.e., block vectors) from the given values for a single block
  std::vector<Functional::VectorType> testPoints(localTestPoints.size()*localTestPoints.size());
  for (size_t i=0; i<testPoints.size(); i++)
    testPoints[i].resize(coefficients.size());

  for (size_t i=0; i<localTestPoints.size(); i++)
    for (size_t j=0; j<localTestPoints.size(); j++)
    {
      testPoints[j*localTestPoints.size()+i][0] = localTestPoints[i];
      testPoints[j*localTestPoints.size()+i][1] = localTestPoints[j];
    }

  // Test whether the functional is convex
  testConvexity(functional, testPoints);

  // Test whether the functional positive 1-homogeneous
  // We abuse the test points as test directions.
  testHomogeneity(functional, testPoints);

  // Test the first derivative at the given test points
  testGradient(functional, testPoints);

  // Test the first derivative at the given test points
  testHessian(functional, testPoints);

  // Test the directional subdifferential at the given test points
  testDirectionalSubdifferential(functional, testPoints);

  // Test the partial subdifferential at the given test points
  testSubDiff(functional, testPoints);

  return 0;
}
