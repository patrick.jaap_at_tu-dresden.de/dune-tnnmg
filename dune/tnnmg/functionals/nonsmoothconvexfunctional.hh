#ifndef DUNE_TNNMG_FUNCTIONALS_NONSMOOTHCONVEXFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_NONSMOOTHCONVEXFUNCTIONAL_HH

#include <limits>

#include <dune/common/fmatrix.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/solvers/common/interval.hh>

#include <dune/tnnmg/functionals/convexfunctional.hh>

namespace Dune {

  namespace TNNMG {


    /** \brief Base class for convex functionals that are only subdifferentiable
     *
     * \tparam LocalVectorTypeTEMPLATE The local vector type
     * \tparam LocalMatrixTypeTEMPLATE The local matrix type of the Hessian matrix
     */
    template < class LocalVectorTypeTEMPLATE=Dune::FieldVector<double,1>, class LocalMatrixTypeTEMPLATE=Dune::FieldMatrix<double,1,1> >
    class NonsmoothConvexFunctional
      : public ConvexFunctional<BlockVector<LocalVectorTypeTEMPLATE> >
    {
    public:
      typedef LocalVectorTypeTEMPLATE LocalVectorType;
      typedef LocalMatrixTypeTEMPLATE LocalMatrixType;

      //! The local block size
      static const int block_size = LocalVectorType::dimension;

      //! The global vector type
      typedef Dune::BlockVector< LocalVectorType > VectorType;

      //! The global matrix type
      typedef Dune::BCRSMatrix< LocalMatrixType > MatrixType;

      //! The index set for sparse matrix construction
      typedef Dune::MatrixIndexSet IndexSet;

      /** \brief Virtual destructor */
      virtual ~NonsmoothConvexFunctional() {};

      //! Add the gradient of the nonlinearity at v to the vector gradient.
      virtual void addGradient(const VectorType& v, VectorType& gradient) const = 0;

      //! Add the Hessian matrix of the nonlinearity at v to the matrix Hessian.
      virtual void addHessian(const VectorType& v, MatrixType& hessian) const = 0;

      //! Add the indices of the Hessian matrix to the index set.
      virtual void addHessianIndices(IndexSet& indices) const = 0;

      /** \brief Compute the domain of the nonlinearity restricted to the line u + t v.
       *
       * This is a default implementation using the (scalar) domain method.
       * If the nonlinearity decouples in the Euclidean directions there is no need
       * to reimplement this.
       *
       * \param[out] dom the output domain
       * \param u base point
       * \param v direction
       */
      virtual void directionalDomain(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& dom) const
      {
        dom[1] =  std::numeric_limits<double>::max();
        dom[0] = -std::numeric_limits<double>::max();

        double z;
        Dune::Solvers::Interval<double> ab;
        for(size_t row=0; row<u.size(); ++row)
        {
          for(int j=0; j<block_size; ++j)
          {
            domain(row, ab, j);
            ab[0] -= u[row][j];
            ab[1] -= u[row][j];
            // build convex constraints along search line
            if (v[row][j]>0.0)
            {
              z = ab[1]/v[row][j];
              dom[1] = std::min(z,dom[1]);

              z = ab[0]/v[row][j];
              dom[0] = std::max(z,dom[0]);
            }
            if (v[row][j]<0.0)
            {
              z = ab[1]/v[row][j];
              dom[0] = std::max(z,dom[0]);

              z = ab[0]/v[row][j];
              dom[1] = std::min(z,dom[1]);
            }
          }
        }
      }

      /** \brief Compute the subdifferential of the nonlinearity restricted to the line u + t v.
       *
       * This is a default implementation using the (scalar) subDiff method.
       * If the nonlinearity decouples in the Euclidean directions there is no need
       * to reimplement this.
       *
       * \param[out] subdifferential the resulting subdifferential
       * \param u base point
       * \param v direction
       */
      virtual void directionalSubDiff(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& subdifferential)
      {
        setVector(u);

        subdifferential[0] = 0.0;
        subdifferential[1] = 0.0;

        Dune::Solvers::Interval<double> Dv;
        for(size_t row=0; row<u.size(); ++row)
        {
          for(int j=0; j<block_size; ++j)
          {
            if (v[row][j]>0.0)
            {
              subDiff(row, u[row][j], Dv, j);
              subdifferential[0] += Dv[0]*v[row][j];
              subdifferential[1] += Dv[1]*v[row][j];
            }
            if (v[row][j]<0.0)
            {
              subDiff(row, u[row][j], Dv, j);
              subdifferential[0] += Dv[1]*v[row][j];
              subdifferential[1] += Dv[0]*v[row][j];
            }
          }
        }
        return;
      }

      /** \brief Set the internal position vector u_pos to v.
       *
       * This is only needed if the nonlinearity does not decouple in the Euclidean directions.
       * If the nonlinearity decouples in the Euclidean directions this can be empty.
       */
      virtual void setVector(const VectorType& v) = 0;

      /** \brief Update the (i,j)-th entry of the internal position vector u_pos to x.
       *
       * If the nonlinearity decouples in the Euclidean directions this can be empty.
       *
       * \param i global index
       * \param j local index
       * \param x new value of the entry (u_pos)_(i,j)
       */
      virtual void updateEntry(int i, double x, int j) = 0;

      /** \brief Compute the subdifferential of the nonlinearity restricted to the
       * line u_pos' +t e_(i,j) at t=0.
       *
       * Here e_(i,j) is the (i,j)-th Euclidean unit vector,
       * and u_pos' is the internal position vector u_pos with the (i,j)-the entry replaced by x.
       * If the nonlinearity decouples in the Euclidean directions this is simply the (i,j)-th
       * component of the subdifferential.
       *
       * \param i global index
       * \param j local index
       * \param x value of the (i,j)-th entry of position to evaluate the nonlinearity at
       * \param[out] D the subdifferential
       */
      virtual void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const = 0;

      /** \brief Return the regularity of the nonlinearity restricted to the
       * line u_pos' +t e_(i,j) at t=0.
       *
       * Here e_(i,j) is the (i,j)-th Euclidean unit vector,
       * and u_pos' is the internal position vector u_pos with the (i,j)-the entry replaced by x.
       * Usually this will be the third derivative or a local Lipschitz constant of the second
       * derivative. Note that if the subdifferential is set-valued at this position, this
       * value will normally be infinity.
       *
       * \param i global index
       * \param j local index
       * \param x value of the (i,j)-th entry of position to evaluate the nonlinearity at
       * \returns a value measuring the regularity
       */
      virtual double regularity(int i, double x, int j) const = 0;

      /** \brief Compute the domain of the nonlinearity restricted to the
       * line t e_(i,j) at t=0 where e_(i,j) is the (i,j)-th Euclidean unit vector.
       *
       * Notice that this does not depend on the position since the nonsmooth
       * part of the nonlinearity must decouple in the Euclidean directions.
       *
       * \param i global index
       * \param j local index
       * \param[out] dom the domain
       */
      virtual void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const = 0;

    };

  }   // namespace TNNMG

}   // namespace Dune

#endif
