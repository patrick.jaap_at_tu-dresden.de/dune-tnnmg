#ifndef DUNE_TNNMG_FUNCTIONALS_CONVEXFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_CONVEXFUNCTIONAL_HH

#include <dune/common/function.hh>

namespace Dune {

  namespace TNNMG {

    /** \brief Convex functional from (R^N)^n to R
     *
     *
     *  \tparam VectorTypeTEMPLATE The type used for elements of (R^N)^n
     */
    template<class VectorTypeTEMPLATE>
    class ConvexFunctional
      : public Dune::VirtualFunction<VectorTypeTEMPLATE, double>
    {
    public:
      //! Export the employed block vector type
      typedef VectorTypeTEMPLATE VectorType;

      /** \brief Evaluates the functional at a given vector u
       *
       *  \param[in] u vector to evaluate the energy at
       *  \returns computed energy
       */
      virtual double operator()(const VectorType& u) const = 0;

      /** \brief Reimplement the 'evaluate' method of the base class
       *
       * Since convex functionals always map to a scalar data type, it is more convenient to
       * have an 'evaluate' method that yields the result through the method return value.
       * This default implementation of 'evaluate' simply calls operator().
       */
      virtual void evaluate(const VectorType& u, double& value) const
      {
        value = this->operator()(u);
      }
    };

  }

}

#endif
