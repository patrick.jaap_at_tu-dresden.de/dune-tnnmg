#ifndef DUNE_TNNMG_FUNCTIONALS_SUMFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_SUMFUNCTIONAL_HH

#include <memory>

#include <dune/common/shared_ptr.hh>
#include "dune/tnnmg/functionals/nonsmoothconvexfunctional.hh"

namespace Dune {

  namespace TNNMG {

    template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
    class SumFunctional: public NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType>
    {
    public:
      typedef NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType> NonlinearityType;

      using NonlinearityType::block_size;

      typedef typename NonlinearityType::VectorType VectorType;
      typedef typename NonlinearityType::MatrixType MatrixType;
      typedef typename NonlinearityType::IndexSet IndexSet;

      SumFunctional(NonlinearityType& phi, NonlinearityType& psi)
      {
        phi_ = Dune::stackobject_to_shared_ptr(phi);
        psi_ = Dune::stackobject_to_shared_ptr(psi);
      }

      SumFunctional(std::shared_ptr<NonlinearityType>& phi, std::shared_ptr<NonlinearityType>& psi) :
      phi_(phi),
      psi_(psi)
      {}

      double operator()(const VectorType& v) const
      {
        double r = 0.0;
        r += (*phi_)(v);
        r += (*psi_)(v);
        return r;
      }

      void addGradient(const VectorType& v, VectorType& gradient) const
      {
        phi_->addGradient(v, gradient);
        psi_->addGradient(v, gradient);
      }

      void addHessian(const VectorType& v, MatrixType& hessian) const
      {
        phi_->addHessian(v, hessian);
        psi_->addHessian(v, hessian);
      }

      void addHessianIndices(IndexSet& indices) const
      {
        phi_->addHessianIndices(indices);
        psi_->addHessianIndices(indices);
      }

      void directionalDomain(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& dom)
      {
        Dune::Solvers::Interval<double> dom2;
        phi_->directionalDomain(u, v, dom);
        psi_->directionalDomain(u, v, dom2);
        dom[0] = std::max(dom[0],dom2[0]);
        dom[1] = std::min(dom[1],dom2[1]);
      }

      void directionalSubDiff(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& sd)
      {
        Dune::Solvers::Interval<double> sd2;
        phi_->directionalSubDiff(u, v, sd);
        psi_->directionalSubDiff(u, v, sd2);
        sd[0] += sd2[0];
        sd[1] += sd2[1];
      }

      void setVector(const VectorType& v)
      {
        phi_->setVector(v);
        psi_->setVector(v);
      }

      void updateEntry(int i, double x, int j)
      {
        phi_->updateEntry(i, x, j);
        psi_->updateEntry(i, x, j);
      }

      void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
      {
        Dune::Solvers::Interval<double> DD;
        phi_->subDiff(i, x, D, j);
        psi_->subDiff(i, x, DD, j);
        D[0] += DD[0];
        D[1] += DD[1];
      }

      double regularity(int i, double x, int j) const
      {
        return phi_->regularity(i, x, j) + psi_->regularity(i, x, j);
      }

      /** \brief The domain of definition, which is the intersection
       * of the domains of definition of the two addends.
       */
      void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
      {
        Dune::Solvers::Interval<double> dom2;
        phi_->domain(i, dom, j);
        psi_->domain(i, dom2, j);
        if (dom2[0] > dom[0])
          dom[0] = dom2[0];
        if (dom2[1] < dom[1])
          dom[1] = dom2[1];
      }

    private:
      std::shared_ptr<NonlinearityType> phi_;
      std::shared_ptr<NonlinearityType> psi_;
    };

  }   // namespace TNNMG

}   // namespace Dune

#endif
