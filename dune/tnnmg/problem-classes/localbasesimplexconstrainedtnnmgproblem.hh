#ifndef LOCAL_BASE_SIMPLEX_CONSTRAINED_TNNMG_PROBLEM_HH
#define LOCAL_BASE_SIMPLEX_CONSTRAINED_TNNMG_PROBLEM_HH

#include <string>
#include <sstream>

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include "dune/solvers/common/staticmatrixtools.hh"

#include "nonlinearity.hh"
#include "convexproblem.hh"
#include "directionalconvexfunction.hh"
#include "blocknonlineartnnmgproblem.hh"
#include "simplexconstrainedgsproblem.hh"
#include "simplexprojectionconvexfunction.hh"

template <class ConvexProblemTypeTEMPLATE>
class LocalBaseSimplexConstrainedTNNMGProblem :
    private BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>,
    public SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>
{
    private:
        // typedef to ease notation
        typedef BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE> BaseClass;


    public:
        typedef ConvexProblemTypeTEMPLATE ConvexProblemType;
        typedef typename ConvexProblemType::NonlinearityType NonlinearityType;
        typedef typename ConvexProblemType::VectorType VectorType;
        typedef typename ConvexProblemType::MatrixType MatrixType;
        typedef typename ConvexProblemType::LocalVectorType LocalVectorType;
        typedef typename ConvexProblemType::LocalMatrixType LocalMatrixType;

        static const int block_size = ConvexProblemType::block_size;
        static const int coarse_block_size = block_size-1;

        struct Linearization
        {
            static const int block_size = coarse_block_size;

            typedef Dune::BCRSMatrix< Dune::FieldMatrix<double,Linearization::block_size,Linearization::block_size> > MatrixType;
            typedef Dune::BlockVector< Dune::FieldVector<double,Linearization::block_size> > VectorType;
            typedef Dune::BitSetVector<Linearization::block_size> BitVectorType;
            typename Linearization::MatrixType A;
            typename Linearization::VectorType b;
            typename Linearization::BitVectorType ignore;

            // transformation data describing the local basis including truncation
            typedef Dune::FieldMatrix<double,BaseClass::Linearization::block_size,Linearization::block_size> LocalTransformationType;
            std::vector<LocalTransformationType> T;
        };


        LocalBaseSimplexConstrainedTNNMGProblem(const Dune::ParameterTree& _config, ConvexProblemType& problem) :
            BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>(_config, problem),
            SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>(_config, problem)
        {
            projectExactly = config.get("project_exactly", true);
            projectByLocalDamping = config.get("project_by_local_damping", false);
        };


        // we need to select the stuff for the generic GS since
        // this type and method appear also in BlockNonlinearGSProblem
        typedef typename SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>::IterateObject IterateObject;
        using SimplexConstrainedGSProblem<ConvexProblemTypeTEMPLATE>::getIterateObject;


        // include computeDampingParameter explcitly since
        // we derive privat from BlockNonlinearTNNMGProblem
        // to be sure to hide anything
        using BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::computeDampingParameter;


        void assembleTruncate(const VectorType& u, Linearization& linearization, const Dune::BitSetVector<block_size>& ignore) const
        {
            // get reference to resolve ambiguity
            ConvexProblemType& problem = BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::problem;

            // we do not need to ignore components explicitly since they are already truncated by BlockNonlinearTNNMGProblem
            linearization.ignore.resize(problem.A.N());
            linearization.ignore.unsetAll();

            // assemble truncated hessian in canonical coordinates
            typename BaseClass::Linearization baseLinearization;
            BaseClass::assembleTruncate(u, baseLinearization, ignore);

            for(int j=0; j<BaseClass::Linearization::block_size; ++j)
                outStream << std::setw(9) << baseLinearization.truncation.countmasked(j);

            // copy sparsity pattern for linearization
            typename NonlinearityType::IndexSet indices(problem.A.N(), problem.A.M());
            indices.import(baseLinearization.A);

            // construct matrix from pattern and initialize it
            indices.exportIdx(linearization.A);
            linearization.A = 0.0;

            linearization.b.resize(u.size());
            linearization.b = 0.0;

            linearization.T.resize(u.size());
            for(int i=0; i<linearization.T.size(); ++i)
            {
                int j1;
                for (j1=0; baseLinearization.truncation[i][j1]; ++j1);

                linearization.T[i] = 0.0;
                int k=0;
                for(int j2=j1+1; j2<block_size; ++j2)
                {
                    if (not(baseLinearization.truncation[i][j2]))
                    {
                        linearization.T[i][j1][k] = 1.0;
                        linearization.T[i][j2][k] = -1.0;
                        ++k;
                    }
                }
            }

            typename BaseClass::Linearization::MatrixType::row_type::Iterator it;
            typename BaseClass::Linearization::MatrixType::row_type::Iterator end;
            for(int row=0; row<baseLinearization.A.N(); ++row)
            {
                linearization.T[row].umtv(baseLinearization.b[row], linearization.b[row]);

                it = baseLinearization.A[row].begin();
                end = baseLinearization.A[row].end();
                for(; it!=end; ++it)
                {
                    int col = it.index();
                    StaticMatrix::addTransformedMatrix(linearization.A[row][col], linearization.T[row], *it, linearization.T[col]);
                }
            }
        };


        void projectCoarseCorrection(
            const VectorType& u,
            const typename Linearization::VectorType& v,
            VectorType& projected_v,
            const Linearization& linearization) const
        {
            // get reference to resolve ambiguity
            ConvexProblemType& problem = BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::problem;

            // transformation (contains truncation) of correction v = Tv*
            projected_v.resize(v.size());
            for(int i=0; i<u.size(); ++i)
                linearization.T[i].mv(v[i], projected_v[i]);

            Bisection bisection;

            // project correction in convex set
            int projected = 0;
            for (int i = 0; i < projected_v.size(); ++i)
            {
                LocalVectorType lower;
                Dune::Solvers::Interval<double> domain;
                double upper_dummy = 0.0;
                bool violation = false;
                for(int j=0; j<block_size; ++j)
                {
                    problem.phi.domain(i, domain, j);
                    lower[j] = domain[0] - u[i][j];
                    if (projected_v[i][j] < lower[j])
                        violation = true;
                }
                if (violation)
                {
                    ++projected;
                    if (projectExactly)
                    {
                        SimplexProjectionConvexFunction<LocalVectorType> projectionSchurComplement(lower, projected_v[i], 0.0);

                        int bisectionCounter = 0;
                        double lambda = bisection.minimize(projectionSchurComplement, 0, 0, bisectionCounter);
                        projectionSchurComplement.getProjection(lambda, projected_v[i]);
                    }
                    if (projectByLocalDamping)
                    {
                        double local_project_factor = 1.0;
                        double component_project_factor = 1.0;
                        for(int j=0; j<block_size; ++j)
                        {
                            if (projected_v[i][j] < lower[j])
                                component_project_factor = lower[j] / projected_v[i][j];
                            if (component_project_factor < local_project_factor)
                                local_project_factor = component_project_factor;
                        }
                        projected_v[i] *= local_project_factor;
                    }
                }
            }
            outStream << std::setw(9) << projected;
        };


        virtual std::string getOutput(bool header=false) const
        {
            if (header)
            {
                outStream.str("");
                for(int j=0; j<BaseClass::Linearization::block_size; ++j)
                    outStream << "  trunc" << std::setw(2) << j;
                outStream << "  project";
            }
            std::string s = outStream.str();
            outStream.str("");
            return s;
        }


    private:
        typedef typename Linearization::LocalTransformationType LocalTransformationType;

        bool projectExactly;
        bool projectByLocalDamping;

    protected:
        // solver parameters
        using BlockNonlinearTNNMGProblem<ConvexProblemTypeTEMPLATE>::config;

        // store output data
        mutable std::ostringstream outStream;
};

#endif

